using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/*
    Object to represent a collection of perlin stack samples
*/
public class PerlinRangeSample
{
    public PerlinRangeSample() { }
    public PerlinRangeSample(float sx, float sy, int xsam, int ysam)
    {
        startX = sx; startY = sy;
        xSamples = xsam; ySamples = ysam;
        Init();
    }


    public float startX, startY, xStride = 1, yStride = 1;
    public int xSamples, ySamples;

    //x,y
    public float[,] samples;

    public bool GetSample(float x, float y, out float val)
    {
        val = -1;
        var xindex = Mathf.RoundToInt((x - startX) / xStride);
        var yindex = Mathf.RoundToInt((y - startY) / yStride);

        if (xindex < xSamples && xindex >= 0 && yindex < ySamples && yindex >= 0)
        {
            val = samples[xindex, yindex];
            return true;
        }

        return false;
    }

    public void Init()
    {
        if(samples == null || samples.GetLength(0) != xSamples || samples.GetLength(1) != ySamples)
        {
            samples = new float[xSamples,ySamples];
        }
        else
        {
            for (int y = 0; y < ySamples; y++) 
            {
                for (int x = 0; x < xSamples; x++)
                {
                    samples[x, y] = 0;
                }
            }
        }
    }


    public void RangeSamplePerlin(PerlinStack stack)
    {
        for (int y = 0; y < ySamples; y++)
        {
            for (int x = 0; x < xSamples; x++)
            {
                samples[x, y] = stack.SamplePerlin(startX + x * xStride, startY + y * yStride);
            }
        }
    }
}