﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class PerlinCutoff
{
    [Range(-0.01f, 1.01f)]
    public float cutoff = 1;
    public PerlinStack[] perlinStacks;

    static public bool IsCutoffMix(float x, float y, PerlinCutoff low, PerlinCutoff high, float mix, ref float perlinAmt, ref float interpCutOff)
    {
        float rpl = low == null ? 0 : (low.perlinStacks == null ? 0 : PerlinStack.MaxSamplePerlinCollection(low.perlinStacks, x, y));
        float rph = high == null ? 0 : (high.perlinStacks == null ? 0 : PerlinStack.MaxSamplePerlinCollection(high.perlinStacks, x, y));

        perlinAmt = Mathf.Lerp(rpl, rph, mix);
        interpCutOff = Mathf.Lerp(low.cutoff, high.cutoff, mix);

        return perlinAmt > interpCutOff;
    }

    public void InsertAllPerlinStacks(HashSet<PerlinStack> stacks)
    {
        for (int i = 0; i < perlinStacks.Length; i++)
        {
            stacks.Add(perlinStacks[i]);
        }
    }
}
[System.Serializable]
public class PerlinCutoffStampCollectionSubRange : PerlinCutoff
{
    public AID.IntRange indexRange;
}