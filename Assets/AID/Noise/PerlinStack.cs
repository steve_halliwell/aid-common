﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

//TODO each layer needs to have it's own instance and not rely on the statics
//TODO needs custom property drawer
[CreateAssetMenu()]
public class PerlinStack : ScriptableObject
{
    public List<PerlinLayer> perlinLayers = new List<PerlinLayer>();

    [NonSerialized]
    [HideInInspector]
    public PerlinRangeSample cachedRange;

    public void SetSeeds(int[] a_seeds)
    {
        for (int i = 0; i < perlinLayers.Count; i++)
        {
            perlinLayers[i].seed = a_seeds[i];
        }
    }

    public static float MaxSamplePerlinCollection(PerlinStack[] manyPerlins, float x, float y)
    {
        //now that perlins are all remapped to 0-1 range min is 0
        float retval = 0;
        for (int i = 0; i < manyPerlins.Length; i++)
        {
            if (manyPerlins[i] != null)
            {
                float outf;

                if (manyPerlins[i].cachedRange != null)
                {
                    manyPerlins[i].cachedRange.GetSample(x, y, out outf);
                }
                else
                {
                    outf = manyPerlins[i].SamplePerlin(x, y);
                }

                retval = Mathf.Max(retval, outf);
            }
        }

        return retval;
    }

    public static float MixPerlinStackPair(PerlinStack a, PerlinStack b, float x, float y, float mix)
    {
        float plow, phigh;

        if (a.cachedRange != null && b.cachedRange != null)
        {
            a.cachedRange.GetSample(x, y, out plow);
            b.cachedRange.GetSample(x, y, out phigh);
        }
        else
        {
            plow = a.SamplePerlin(x, y);
            phigh = b.SamplePerlin(x, y);
        }
        return Mathf.Lerp(plow, phigh, mix);
    }

    public float SamplePerlin(float x, float y)
    {
        //TODO smarter bounds calc than just max
        float ret = 0;
        float min = 0;
        float max = 0;

        for (int i = 0; i < perlinLayers.Count; i++)
        {
            float n = perlinLayers[i].Sample(x, y, 0);

            var mag = perlinLayers[i].outputScale;

            //math op the ret
            switch (perlinLayers[i].mathOp)
            {
                case PerlinLayer.MathOp.Add:
                    ret += n;
                    min = Mathf.Max(-perlinLayers[i].maxMagnitude, min + 0);
                    max = Mathf.Min(perlinLayers[i].maxMagnitude, max + mag);
                    break;
                case PerlinLayer.MathOp.Sub:
                    ret -= n;
                    min = Mathf.Max(-perlinLayers[i].maxMagnitude, min - mag);
                    max = Mathf.Min(perlinLayers[i].maxMagnitude, max + 0);
                    break;
                case PerlinLayer.MathOp.Mul:
                    ret *= n;
                    min = Mathf.Max(-perlinLayers[i].maxMagnitude, Mathf.Min(0, min * mag));
                    max = Mathf.Min(perlinLayers[i].maxMagnitude, Mathf.Max(0, max * mag));
                    break;
                case PerlinLayer.MathOp.Div:
                    ret /= n;
                    min = Mathf.Max(-perlinLayers[i].maxMagnitude, Mathf.Min(0, min / mag));
                    max = Mathf.Min(perlinLayers[i].maxMagnitude, Mathf.Max(0, max / mag));
                    break;
                case PerlinLayer.MathOp.Mod:
                    ret %= n;
                    min = Mathf.Max(-perlinLayers[i].maxMagnitude, 0);
                    max = Mathf.Min(perlinLayers[i].maxMagnitude, n);
                    break;
                case PerlinLayer.MathOp.Pow:
                    ret = Mathf.Pow(ret, n);
                    min = Mathf.Max(-perlinLayers[i].maxMagnitude, Mathf.Min(0, Mathf.Pow(min, n)));
                    max = Mathf.Min(perlinLayers[i].maxMagnitude, Mathf.Max(0, Mathf.Pow(max, n)));
                    break;
                case PerlinLayer.MathOp.Min:
                    ret = Mathf.Min(ret, n);
                    min = Mathf.Max(-perlinLayers[i].maxMagnitude, 0);
                    max = Mathf.Min(perlinLayers[i].maxMagnitude, n);
                    break;
                case PerlinLayer.MathOp.Max:
                    ret = Mathf.Max(ret, n);
                    min = Mathf.Max(-perlinLayers[i].maxMagnitude, 0);
                    max = Mathf.Min(perlinLayers[i].maxMagnitude, n);
                    break;
                default:
                    break;
            }
            ret = Mathf.Clamp(ret, min, max);
        }

        return (ret - min) / (max - min);
    }

    public void FillCache()
    {
        if (cachedRange != null)
            cachedRange.RangeSamplePerlin(this);
    }

    public float GetCachedSample(float x, float y, float defaultForOutOfBounds)
    {
        float retval = defaultForOutOfBounds;
        if(cachedRange != null)
        {
            cachedRange.GetSample(x, y, out retval);
        }

        return retval;
    }
}
