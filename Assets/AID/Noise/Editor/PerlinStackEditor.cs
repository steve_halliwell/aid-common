﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace AID
{
    [CustomEditor(typeof(PerlinStack))]
    [CanEditMultipleObjects]
    public class PerlinStackEditor : Editor
    {

        public override void OnInspectorGUI()
        {
            PerlinStack my = (PerlinStack)target;

            //EditorGUIUtility.LookLikeControls();

            if (GUILayout.Button("Open In Preview Window"))
            {
                PerlinStackPreview.Init();
                PerlinStackPreview.inst.ps = my;
            }


            DrawDefaultInspector();
        }

    }
}