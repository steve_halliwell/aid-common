﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace AID
{
    public class PerlinStackPreview : EditorWindow
    {
        readonly static int WINDOW_MIN_HEIGHT = 256;
        readonly static int WINDOW_MIN_WIDTH = 256;
        readonly static int WINDOW_PADDING = 5;
        readonly static int CELLS_PER_UPDATE = 1000;

        int yindex;

        public PerlinStack ps;
        public Vector2 previewSize = new Vector2(20, 100);
        public Vector2 previewOffset;
        public int previewSeed;
        public Texture2D tex;
        public Color32[] colors;

        private volatile int backgroundThreadCounter = 0;
        PerlinRangeSample range;

        public static PerlinStackPreview inst;

        void OnEnable()
        {
            EditorApplication.update += Update;
            backgroundThreadCounter = 0;
        }

        void OnDisable()
        {
            EditorApplication.update -= Update;
        }


        // Add to the Window menu
        [MenuItem("Window/PerlinStackPreview")]
        public static void Init()
        {
            // Get existing open window or if none, make a new one:
            PerlinStackPreview window = (PerlinStackPreview)EditorWindow.GetWindow(typeof(PerlinStackPreview));
            inst = window;
            window.Show();
            window.Focus();
            window.minSize = new Vector2(WINDOW_MIN_WIDTH + 2 * WINDOW_PADDING, WINDOW_MIN_HEIGHT + 2 * WINDOW_PADDING);
        }

        void OnGUI()
        {
            if (ps == null)
            {
                GUILayout.Label("No Perlin Stack Set");
            }

            ps = (PerlinStack)EditorGUILayout.ObjectField(ps, typeof(PerlinStack), true);
            previewSeed = EditorGUILayout.IntField("Preview seed", previewSeed);

            Vector2 previewSizeNew = EditorGUILayout.Vector2Field("Preview Size", previewSize);
            previewOffset = EditorGUILayout.Vector2Field("Preview starting position", previewOffset);


            if (previewSizeNew != previewSize || tex == null)
            {
                previewSize = previewSizeNew;
                tex = new Texture2D((int)previewSize.x, (int)previewSize.y, TextureFormat.RGBA32, false);
                colors = new Color32[tex.width * tex.height];
                tex.anisoLevel = 0;
                tex.filterMode = FilterMode.Point;
            }

            var startAtY = 7 * EditorGUIUtility.singleLineHeight;
            GUI.DrawTexture(new Rect(WINDOW_PADDING, startAtY, Screen.width - WINDOW_PADDING, Screen.height - startAtY - WINDOW_PADDING), tex, ScaleMode.ScaleToFit);
        }

        public void Update()
        {
            if (ps == null)
            {
                return;
            }

            int cells = 0;
            int maxY = CELLS_PER_UPDATE / tex.width;

            if (System.Threading.Interlocked.Equals(backgroundThreadCounter, 0))
            {
                //prime the stack
                int[] seeds = new int[ps.perlinLayers.Count];
                for (int i = 0; i < seeds.Length; i++)
                {
                    seeds[i] = previewSeed;
                }
                ps.SetSeeds(seeds);

                if (yindex >= tex.height)
                    yindex = 0;


                System.Threading.Interlocked.Increment(ref backgroundThreadCounter);

                range = new PerlinRangeSample(0 + previewOffset.x, yindex + previewOffset.y, tex.width, maxY);

                ps.cachedRange = range;

                //not actually doing anything useful here but proving a point
                var t = new System.Threading.Thread(() =>
                {
                    ps.FillCache();
                    System.Threading.Interlocked.Exchange(ref backgroundThreadCounter, -1);
                });
                t.Start();
                return;
            }


            if (!System.Threading.Interlocked.Equals(backgroundThreadCounter, -1))
                return;

            //update texture
            for (int y = 0; yindex < tex.height && cells < maxY; y++, yindex++, cells++)
            {

                for (int x = 0; x < tex.width; x++)
                {
                    float psval = ps.GetCachedSample(x + previewOffset.x, yindex + previewOffset.y, -1);
                    //psval = range.samples[x,y];// ps.SamplePerlin(x + previewOffset.x, y + previewOffset.y);
                    //tex.SetPixel(i, j, Color.white * psval);
                    byte inten = (byte)(255 * psval);
                    colors[yindex * tex.width + x] = new Color32(inten, inten, inten, 255);
                }
            }

            System.Threading.Interlocked.Exchange(ref backgroundThreadCounter, 0);
            tex.SetPixels32(colors);
            tex.Apply();

            Repaint();
        }
    }
}