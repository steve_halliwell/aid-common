﻿using LibNoise.Generator;
using System;

public static class LibNoiseStatics
{
    
    [ThreadStatic]public static Billow _billow;
    public static Billow billow 
    {
        get
        {
            if(_billow == null)
                _billow = new Billow();

            return _billow;
        }
    }
    [ThreadStatic]public static Perlin _perlin;
    public static Perlin perlin 
    {
        get
        {
            if(_perlin == null)
                _perlin = new Perlin();

            return _perlin;
        }
    }
    [ThreadStatic]public static RidgedMultifractal _ridgedMultiFractal;
    public static RidgedMultifractal ridgedMultiFractal 
    {
        get
        {
            if(_ridgedMultiFractal == null)
                _ridgedMultiFractal = new RidgedMultifractal();

            return _ridgedMultiFractal;
        }
    }
    [ThreadStatic]public static Voronoi _voronoi;
    public static Voronoi voronoi 
    {
        get
        {
            if(_voronoi == null)
                _voronoi = new Voronoi();

            return _voronoi;
        }
    }
}
