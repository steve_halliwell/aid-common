﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[System.Serializable]
public class PerlinLayer
{
    public float uniformMagnify = 1;
    public Vector2 magnifySkew = new Vector2(0, 0);
    public float outputScale = 1;
    [Tooltip("Will clamp values to be no larger than max magnitude, recommended when using multiply and divides")]
    public float maxMagnitude = 1;

    [Tooltip("Remap Perlin values as if between 0-1")]
    public AnimationCurve remapCurve = AnimationCurve.Linear(0,0,1,1);

    [System.NonSerialized]
    public int seed;

    public enum MathOp
    {
        Add,
        Sub,
        Mul,
        Div,
        Mod,
        Pow,
        Min,
        Max
    }
    public MathOp mathOp;

    public enum NoiseAlg
    {
        Perlin,
        Billow,
        RidgedMultiFractal,
        Voronoi
    }
    public NoiseAlg noiseAlg;

    public float Sample(float x, float y, float z)
    {

        if (uniformMagnify <= 0)
            uniformMagnify = float.MinValue;

        float n = 0;
        float xs = (uniformMagnify + magnifySkew.x);
        if (xs != 0)
            xs = 1.0f / xs;
        float ys = (uniformMagnify + magnifySkew.y);
        if (ys != 0)
            ys = 1.0f / ys;

        x *= xs;
        y *= ys;
        
        switch (noiseAlg)
        {
            //case PerlinLayer.NoiseAlg.Simplex:
            //    n = SimplexNoise.Noise.Generate(x * perlinLayers[i].inputScale.x, y * perlinLayers[i].inputScale.y);
            //    break;
            //case PerlinLayer.NoiseAlg.PerlinUnity:
            //    n = Mathf.PerlinNoise(x * perlinLayers[i].inputScale.x, y * perlinLayers[i].inputScale.y);
            //    break;
            case PerlinLayer.NoiseAlg.Billow:
                LibNoiseStatics.billow.Seed = seed;
                n = (float)LibNoiseStatics.billow.GetValue(x, y, z);
                break;
            case PerlinLayer.NoiseAlg.Perlin:
                LibNoiseStatics.perlin.Seed = seed;
                n = (float)LibNoiseStatics.perlin.GetValue(x, y, z);
                break;
            case PerlinLayer.NoiseAlg.RidgedMultiFractal:
                LibNoiseStatics.ridgedMultiFractal.Seed = seed;
                n = (float)LibNoiseStatics.ridgedMultiFractal.GetValue(x, y, z);
                break;
            case PerlinLayer.NoiseAlg.Voronoi:
                LibNoiseStatics.voronoi.Seed = seed;
                n = (float)LibNoiseStatics.voronoi.GetValue(x, y, z);
                break;
            default:
                break;
        }


        //perlin is -1,1
        //remap to 0-1 get from curve
        n += 1;
        n /= 2;
        n = remapCurve.Evaluate(n);
        var mag = outputScale;
        n *= mag;


        return n;
    }
}
